import mongoose from "mongoose";
import { config as dotenvConfig } from "dotenv";

function getCollectionName() {
  dotenvConfig();
  return process.env.NODE_ENV === "test" ? process.env.JEST_COLLECTION_NAME : process.env.COLLECTION_NAME;
}

interface IGeoLocation {
  type: string;
  coordinates: number[];
}

const pointSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: ["Point"],
    required: true,
  },
  coordinates: {
    type: [Number],
    required: true,
  },
});

export interface IStatsQuery extends mongoose.QueryOptions {
  country?: string;
  date?: string;
}

export interface ITest extends mongoose.Document {
  patientName: string;
  location: IGeoLocation;
  age: number;
  testResult: string;
  testDate: Date;
  country: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export const TestSchema = new mongoose.Schema(
  {
    patientName: String,
    location: {
      type: pointSchema,
      required: true,
    },
    age: Number,
    testResult: String,
    testDate: Date,
    country: String,
  },
  { timestamps: true }
);

const Test = mongoose.model<ITest>(getCollectionName(), TestSchema);

export default Test;
