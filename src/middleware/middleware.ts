import { AnySchema } from "yup";
import { Request, Response, NextFunction } from "express";
import { log } from "../util/util";
import dayjs from "dayjs";

/**
 * Transform date strings to Date type
 */
export const transformDateString = (originalDate: string) => {

  return dayjs(originalDate, "YYYY-MM-DD", true).isValid()
    ? new Date(originalDate)
    : new Date("");
};

/**
 * Trims all whitespaces from front and back of parametsr in body
 */
export const trimmer = (req: Request, res: Response, next: NextFunction) => {
  for (const [key, value] of Object.entries(req.body)) {
    if (typeof req.body[key] === "string")
      req.body[key] = (value as string).trim();
  }
  next();
};

/**
 * Validates all client inputs for body, params and query match schema
 */
export const validateRequest =
  (schema: AnySchema) =>
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      // Ensure our testResult input is in upper case
      if (req.body.testResult)
        req.body.testResult = req.body.testResult.toUpperCase();

      await schema.validate({
        body: req.body,
        query: req.query,
        params: req.params,
      });

      return next();
    } catch (err) {
      log.error(err);
      return res.status(400).send(err.errors);
    }
  };
