import TestController from "./controller/test.controller";
import express from "express";

export function createApp() {
  const app = express();
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use("/api/tests", TestController.getRouter());
  
  return app;
}
