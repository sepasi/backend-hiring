import { transformDateString } from "../middleware/middleware";
import { object, string, number, date, mixed } from "yup";

// This file defines schema for client body, query, and paramters related to tests related API calls

const bodySchema = {
  body: object({
    patientName: string()
      .required("patientName is required")
      .min(5, "patientName must be a minimum of 3 charecters")
      .max(40, "patientName must be a maximum of 40 charecters")
      .trim(),
    age: number().required("age is required").min(1).max(110),
    longitude: number().required("longitude is required").min(-180).max(180),
    latitude: number().required("latitude is required").min(-90).max(90),
    testResult: mixed()
      .oneOf(["POSITIVE", "NEGATIVE"])
      .required("testResult <POSITIVE/NEGATIVE> is required"),
    testDate: date().required("testDate is required in format YYYY-MM-DD").transform(transformDateString).max(new Date())
  }),
};

const querySchema = {
  query: object({
    country: string().trim().matches(/^[a-zA-Z ]*$/, "Country name must be alphabetical").max(56),
    date: date().transform(transformDateString).max(new Date()),
  }),
}

const paramsSchema = {
  params: object({
    testId: string().required("testId is required.").trim(),
  }),
};

export const createTestSchema = object({
  ...bodySchema,
});

export const getStatsSchema = object({
  ...querySchema,
});

export const updateTestSchema = object({
  ...paramsSchema,
  ...bodySchema,
});

export const deleteTestSchema = object({
  ...paramsSchema,
});
