import node_geocoder from "node-geocoder";
import logger from "pino";
import dayjs from "dayjs";

export const log = logger({
  prettyPrint: true,
  base: { pid: false },
  timestamp: () => `,"time":"${dayjs().format()}"`,
});

const options: node_geocoder.Options = {
  provider: "openstreetmap",
};

export const geoCoder = node_geocoder(options);