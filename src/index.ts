import { config as dotenvConfig } from "dotenv";
import { connect } from "mongoose";
import { createApp } from "./app";
import {log} from "./util/util";

dotenvConfig();

const dbUri = process.env.DB_URI;

connect(dbUri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
})
  .then(() => log.info("Connected to database"))
  .catch((e) => {
    log.error("Unable to connect to DB", e);
    process.exit(1);
  });

const app = createApp();
const port = Number(process.env.PORT);

app.listen(port, () => console.log(`App listening on :${port}`));
