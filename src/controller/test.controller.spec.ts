import { config as dotenvConfig } from "dotenv";
import { log } from "../util/util";
import request from "supertest";
import { createApp } from "../app";
import { omit } from "lodash";
import { connect, disconnect, connection } from "mongoose";
import TestService from "../services/test.service";
import { ITest } from "src/model/test.model";


describe("TestController", () => {

  const mockData = {
    patientName: "Sam Sepasi",
    age: "29",
    latitude: "38.8951",
    longitude: "-77.0364",
    testResult: "POSITIVE",
    testDate: "2021-07-06",
  };

  const sanitizedData = {
    patientName: "Jackson Johnson",
    age: 29,
    location: {
      coordinates: [-117.21113426545688, 32.80228179062758],
      _id: "60e3c7cfeba9061c8e38b692",
      type: "Point",
    },
    country: "United States",
    testResult: "NEGATIVE",
    testDate: new Date(),
  };

  beforeAll(() => {
    dotenvConfig();
      
    const dbUri = process.env.DB_URI;
  
    connect(dbUri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    })
      .then(() => log.info("Connected to database"))
      .catch((e) => {
        log.error("Unable to connect to DB", e);
        process.exit(1);
      });
  
    // Populate the db with some dummy data.
    let data = { ...sanitizedData};
  
    for (let i = 0; i < 50; ++i) {
      data.testDate.setDate(data.testDate.getDate()-1);
      data.testResult === "POSITIVE"
        ? (data.testResult = "NEGATIVE")
        : (data.testResult = "POSITIVE");
      const test = TestService.createTest(data);
    }
  
    jest.setTimeout(20000);
  
  });
  
  afterAll((done) => {
    // Optionally drop collection after all tests.
  })

  it("POST /api/tests missing parameters returns 400", async (done) => {
    const incompleteData = [
      omit(mockData, ["latitude"]),
      omit(mockData, ["longitude"]),
      omit(mockData, ["patientName"]),
      omit(mockData, ["age"]),
      omit(mockData, ["testResult"]),
    ];

    for (const data of incompleteData) {
      request(createApp())
        .post("/api/tests")
        .send(data)
        .set("Content-type", "application/json")
        .expect(400, done);
    }
  });

  it("POST /api/tests with invalid parameters returns 400", async (done) => {
    let badName = { ...mockData };
    badName.patientName = "   "; // A name with just whitespace.
    let badAge = { ...mockData }; // Age passed as string.
    badAge.age = "twenty";
    const incompleteData = [badName, badAge];

    for (const data of incompleteData) {
      request(createApp())
        .post("/api/tests")
        .send(data)
        .set("Content-type", "application/json")
        .expect(400, done);
    }
  });

  it("POST /api/tests with valid parameters returns 201", async (done) => {
    request(createApp())
      .post("/api/tests")
      .send(mockData)
      .set("Content-type", "application/json")
      .expect("Content-Type", /json/)
      .expect(201)
      .then((res) => {
        expect(res.body).toHaveProperty("_id");
        expect(res.body.patientName).toEqual(mockData.patientName);
        done();
      })
      .catch((err) => done(err));
  });

  it("GET /api/tests Returns 200 and data.", async (done) => {
    request(createApp())
      .get(`/api/tests`)
      .expect("Content-Type", /json/)
      .expect(200)
      .then((res) => {
        expect(res.body.length).toBeGreaterThan(0);
        done();
      })
      .catch((err) => done(err));
  });

  it("GET /api/tests/:id Returns 200 and expected data.", async (done) => {
    const test: ITest = await TestService.createTest(sanitizedData);
    const testId: String = test._id;

    request(createApp())
      .get(`/api/tests/${testId}`)
      .expect("Content-Type", /json/)
      .expect(200)
      .then((res) => {
        expect(JSON.stringify(res.body._id)).toEqual(JSON.stringify(testId));
        expect(res.body.patientName).toEqual(sanitizedData.patientName);
        expect(res.body.age).toEqual(sanitizedData.age);
        expect(res.body.location).toEqual(sanitizedData.location);
        expect(res.body.country).toEqual(sanitizedData.country);
        expect(res.body.testResult).toEqual(sanitizedData.testResult);
        done();
      })
      .catch((err) => done(err));
  });

  it("PUT /api/tests with valid update returns 200.", async (done) => {
    let updateData = { ...mockData };
    updateData.patientName = "updated name";
    updateData.testResult = "POSITIVE";

    const test: ITest = await TestService.createTest(sanitizedData);
    const testId: String = test._id;

    request(createApp())
      .put(`/api/tests/${testId}`)
      .send(updateData)
      .set("Content-type", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((res) => {
        expect(JSON.stringify(res.body._id)).toEqual(JSON.stringify(testId));
        expect(res.body.patientName).toEqual(updateData.patientName);
        expect(res.body.testResult).toEqual(updateData.testResult);
        done();
      })
      .catch((err) => done(err));
  });

  it("DEL /api/tests/:id with bad testId returns 404.", async (done) => {
    const testId: String = "60df00bd659845da69e8e000";

    request(createApp()).del(`/api/tests/${testId}`).expect(404, done);
  });

  it("DEL /api/tests/:id with valid testId returns 200.", async (done) => {
    const test: ITest = await TestService.createTest(sanitizedData);
    const testId: String = test._id;

    request(createApp()).del(`/api/tests/${testId}`).expect(200, done);
  });

  it("GET /api/tests/stats returns 200 stats data.", async (done) => {
    request(createApp())
      .get(`/api/tests/stats`)
      .expect("Content-Type", /json/)
      .expect(200)
      .then((res) => {
        expect(res.body).toHaveProperty("countries");
        expect(res.body).toHaveProperty("dates");
        done();
      })
      .catch((err) => done(err));
  });

  it("GET /api/tests/stats?query return 200 with stats data.", async (done) => {
    request(createApp())
      .get(`/api/tests/stats?country=england&date=2021-07-06`)
      .expect("Content-Type", /json/)
      .expect(200)
      .then((res) => {
        expect(res.body).toHaveProperty("countries");
        expect(res.body).toHaveProperty("dates");
        done();
      })
      .catch((err) => done(err));
  });

  /* Following tests are for coverage purposese, database will be disconnected to excercise path
  * These tests add a lot of time to overal test due to timeout of database calls, only having 1 test
  */

  it("POST /api/tests no connection to DB returns 500.", async (done) => {
    disconnect();
    request(createApp())
      .post("/api/tests")
      .send(mockData)
      .set("Content-type", "application/json")
      .expect(500, done);
  });
});
