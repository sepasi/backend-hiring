import { Request, Response, Router } from "express";
import { validateRequest, trimmer } from "../middleware/middleware";
import { log, geoCoder } from "../util/util";
import TestService from "../services/test.service";
import {
  createTestSchema,
  deleteTestSchema,
  getStatsSchema,
  updateTestSchema,
} from "../schema/test.schema";
import { IStatsQuery } from "src/model/test.model";

interface ITestBody {
  patientName: string;
  location: {
    type: string;
    coordinates: number[];
  };
  age: number;
  country: string;
  testResult: string;
  testDate: Date;
}

export default class TestController {
  /**
   * Routes endpoints to appropriate validation and controller functions
   */
  static getRouter(): Router {
    const router = Router();

    // Get test stats by query
    router.get(
      "/stats",
      validateRequest(getStatsSchema),
      TestController.getStatHandler
    );

    // Create a Test
    router.post(
      "/",
      trimmer,
      validateRequest(createTestSchema),
      TestController.createTestHandler
    );

    // Get all tests
    router.get("/", TestController.getTestHandler);

    // Get test by id
    router.get("/:testId", TestController.getTestByIdHandler);

    // Update patient info
    router.put(
      "/:testId",
      trimmer,
      validateRequest(updateTestSchema),
      TestController.updateTestHandler
    );

    // Delete a test
    router.delete(
      "/:testId",
      validateRequest(deleteTestSchema),
      TestController.deleteTestHandler
    );

    return router;
  }

  /**
   *  Returns statisicts on number of tests per day and per country based on query
   *  If no query provided, this will return daily tests stats on all tests
   */
  static async getStatHandler(req: Request, res: Response) {
    try {
      let queryObj: { date: string; country: string } = {
        date: null,
        country: null,
      };

      if (req.query.country != undefined)
        queryObj.country = req.query.country as string;
      if (req.query.date != undefined) queryObj.date = req.query.date as string;

      const countries = await TestService.getStatByCountry(
        queryObj as IStatsQuery
      );
      const dates = await TestService.getStatByDates(queryObj as IStatsQuery);

      res.status(200).json({ countries, dates });
    } catch (err) {
      log.error(err);
      res.status(500).json({ error: err });
    }
  }

  /**
   * Takes in client request with body to create new test
   * Returns new test object with id
   */
  static async createTestHandler(req: Request, res: Response) {
    try {
      // Middleware has validated params and body attributes exists.
      const body = req.body;
      const location = await geoCoder.reverse({
        lat: req.body.latitude,
        lon: req.body.longitude,
      });

      const testBody: ITestBody = {
        patientName: body.patientName,
        age: body.age,
        location: {
          type: "Point",
          coordinates: [body.longitude, body.latitude],
        },
        country: location[0].country,
        testResult: body.testResult,
        testDate: body.testDate as Date,
      };

      const newTest = await TestService.createTest(testBody);

      res.status(201).json(newTest);
    } catch (err) {
      log.error(err);
      res.status(500).json({ error: err });
    }
  }

  /**
   * Takes paramter and body to find and update test
   * Returns updated test object (200) or 404 or 500 for bad request
   */
  static async updateTestHandler(req: Request, res: Response) {
    try {
      // Middleware has validated params and body attributes exists.
      const testId = req.params.testId;
      const body = req.body;

      const test = await TestService.findTest({ _id: testId });

      if (!test)
        return res
          .sendStatus(404)
          .json({ error: `Test with id ${testId} not found` });

      const location = await geoCoder.reverse({
        lat: req.body.latitude,
        lon: req.body.longitude,
      });

      const testBody: ITestBody = {
        patientName: body.patientName,
        age: body.age,
        location: {
          type: "Point",
          coordinates: [body.longitude, body.latitude],
        },
        country: location[0].country,
        testResult: body.testResult,
        testDate: body.testDate as Date,
      };

      const updatedTest = await TestService.findAndUpdateTest(
        { _id: testId },
        testBody,
        { new: true }
      );

      res.status(200).json(updatedTest);
    } catch (err) {
      log.error(err);
      res.status(500).json({ error: err });
    }
  }

  /**
   * Returns all tests matching any query in database
   */
  static async getTestHandler(req: Request, res: Response) {
    try {
      const tests = await TestService.findTests({});

      res.status(200).json(tests);
    } catch (err) {
      log.error(err);
      res.status(500).json({ error: err });
    }
  }

  /**
   * Takes id from paramter to find a specific test
   * Returns found test object (200) or 404 or 500 for bad request
   */
  static async getTestByIdHandler(req: Request, res: Response) {
    try {
      const testId = req.params.testId;
      const test = await TestService.findTest({ _id: testId });

      if (!test)
        return res
          .status(404)
          .json({ error: `Test with id ${testId} not found` });

      res.status(200).json(test);
    } catch (err) {
      log.error(err);
      res.status(500).json({ error: err });
    }
  }

  /**
   * Takes id from paramter to find a specific test and deletes one
   * Returns status 200 for completed or 404 or 500 for bad request
   */
  static async deleteTestHandler(req: Request, res: Response) {
    try {
      const testId = req.params.testId;

      const test = await TestService.findTest({ _id: testId });

      if (!test)
        return res
          .status(404)
          .json({ error: `Test with id ${testId} not found` });

      await TestService.deleteTest({ _id: testId });

      res.sendStatus(200);
    } catch (err) {
      log.error(err);
      res.status(500).json({ error: err });
    }
  }
}
