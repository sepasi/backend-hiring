import {
  QueryOptions,
  FilterQuery,
  UpdateQuery,
  DocumentDefinition,
} from "mongoose";
import Test, { IStatsQuery, ITest } from "../model/test.model";

/**
 * TestService class handles all databse calls for Tests
 */
export default class TestService {
  /**
   * Uses mongo create to create new item
   */
  static async createTest(input: DocumentDefinition<ITest>) {
    return Test.create(input);
  }

  /**
   * Uses mongo findOne to fetch item matching _id
   */
  static async findTest(
    query: FilterQuery<ITest>,
    options: QueryOptions = { lean: true }
  ) {
    return Test.findOne(query, {}, options);
  }

  /**
   * Uses mongo find to fetch items matching query or all if no query
   */
  static async findTests(
    query: FilterQuery<ITest>,
    options: QueryOptions = { lean: true }
  ) {
    return Test.find(query, {}, options);
  }

  /**
   * Uses mongo findOneandUpdate to update item matching _id with new object
   */
  static async findAndUpdateTest(
    query: FilterQuery<ITest>,
    update: UpdateQuery<ITest>,
    options: QueryOptions
  ) {
    return Test.findOneAndUpdate(query, update, options);
  }

  /**
   * Calls mongo deleteOne function to delete item matching _id
   */
  static async deleteTest(query: FilterQuery<ITest>) {
    return Test.deleteOne(query);
  }

  /**
   * Returns all distinct values for a given attribute
   * ** NOTE: Currently not being used, but can be used for future
   * if cleint wants a list of all unique countries, age or etc in DB **
   */
  static async getDistinct(attributeName: string) {
    return Test.distinct(attributeName);
  }

  /**
   * Returns total tests and total positive tests grouped by date
   * If dateString is passed in, it will only return stats for that date
   * Note: this aggregate call may be improved by avoiding dateToString call,
   * instead, compare lte / gte of start to end of date objects.
   */
  static async getStatByDates(
    query: IStatsQuery = { date: null, country: null }
  ) {

    let dateFilter = {};
    if (query.date != null) {
      dateFilter = {
        $expr: {
          $eq: [
            query.date,
            { $dateToString: { format: "%Y-%m-%d", date: "$testDate" } },
          ],
        },
      } as Object;
    }

    return Test.aggregate([
      { $match: { ...dateFilter } },
      {
        $group: {
          _id: { $dateToString: { format: "%Y-%m-%d", date: "$testDate" } },
          totalTests: { $sum: 1 },
          totalPositive: {
            $sum: { $cond: [{ $eq: ["$testResult", "POSITIVE"] }, 1, 0] },
          },
        },
      },
      { $sort: { _id: 1 } },
      {
        $project: {
          _id: 0,
          date: "$_id",
          totalTests: 1,
          totalPositive: 1,
        },
      },
    ]);
  }

  /**
   * Returns total tests and total positive tests grouped by country
   * If country is passed in, it will only return stats for that country
   *
   */
  static async getStatByCountry(
    query: IStatsQuery = { date: null, country: null }
  ) {
    let countryFilter = {};
    if (query.country != null) {
      countryFilter = { country: { $regex: query.country, $options: 'i' } };
    }

    return Test.aggregate([
      { $match: { ...countryFilter } },
      {
        $group: {
          _id: "$country",
          totalTests: { $sum: 1 },
          totalPositive: {
            $sum: { $cond: [{ $eq: ["$testResult", "POSITIVE"] }, 1, 0] },
          },
        },
      },
      { $sort: { _id: 1 } },
      {
        $project: {
          _id: 0,
          country: "$_id",
          totalTests: 1,
          totalPositive: 1,
        },
      },
    ]);
  }
}
