## TREELINE COVID 19 TEST API
 
**This is a simple REST API to allow user to enter COVID 19 test results for patients.**

**API Endpoints:**

* api/tests : (POST, GET) - create test, gets all tests. Body of Post is expected as:
    * patientName : Name of patient
    * age: Age of patient
    * testResult: result of COVID 19 test <POSITIVE> or <NEGATIVE>
    * latitude: latitude of location of patient/ test taken
    * longitude: longitude of location of patient/ test taken
 
* api/tests/id : (GET, PUT, DELETE) - get, update, delete test by id
* api/tests/stats: (GET) - Optional Query supported: country: string, date: dateString. By default returns total tests and total positive tests per day, and per country

**Start App**

Setup Docker: docker-compose -f docker-compose.yml up  
Setup npm: npm install  
Start application: npm start

**JEST**

Run test: npm test  
Jest/Supertest creates some dummy documents in a test specific collection (jest-tests) to run tests on.

**Assumptions:** 

For simplicity it is assumed there is only 1 test per patient. You can add additional tests for a patient but this adds a new document to the DB.

 
**Areas for future improvements:**
 
* Save patient DOB and other unique information to be able to query and support additional tests for a patient
* Save testDate and testResults in an array of objects to allow for multiple tests per patient. A patient is not expected to have a very large number of COVID 19 tests
* Add additional input validation / sanitization to the API
* Use jest mock features instead of real database calls to reduce time of tests as project gets larger
* Add support to allow clients to request for number of pages, entries per page etc
* Allow queries for a range or array of dates, and countries
* Add user authentication so only authorized users can add, edit, or view the content. This would use username & passwords to create users and sessions with help jwt or similar libraries
* For more accurate time keeping, keep track of local times
* Expand on testing, currently tests are doing basic checks